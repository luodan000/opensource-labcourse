# 开源实验课

#### 介绍
开源实验课课程管理仓库

#### 交流
如果您在学习过程中，有任何疑问或需要帮助的，请在Issue区留言，openLooKeng社区运营团队尽力为您解答


#### 参考链接

1. openLooKeng官网：https://openlookeng.io/zh-cn
2. openLooKeng社区博客提交攻略：https://openlookeng.io/zh-cn/blogguidance.html
3. gitee工作流程参考：https://gitee.com/openeuler/community/blob/master/zh/contributors/Gitee-workflow.md 
4. openLooKeng社区Bot commands：https://gitee.com/openlookeng/community/blob/master/command.md
